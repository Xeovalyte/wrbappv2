import { mount } from '@vue/test-utils'
import Home from '@/views/Home.vue'

describe('Home.vue', () => {
  it('renders tab 1 Tab1Page', () => {
    const wrapper = mount(Home)
    expect(wrapper.text()).toMatch('Home')
  })
})
