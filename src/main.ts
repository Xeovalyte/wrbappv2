import { createApp } from 'vue'
import App from './App.vue'
import router from './router';

import { IonicVue } from '@ionic/vue';
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getFirestore } from "firebase/firestore";

/* Core CSS required for Ionic components to work properly */
import '@ionic/vue/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/vue/css/normalize.css';
import '@ionic/vue/css/structure.css';
import '@ionic/vue/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/vue/css/padding.css';
import '@ionic/vue/css/float-elements.css';
import '@ionic/vue/css/text-alignment.css';
import '@ionic/vue/css/text-transformation.css';
import '@ionic/vue/css/flex-utils.css';
import '@ionic/vue/css/display.css';

/* Theme variables */
import './theme/variables.css';

const app = createApp(App)
  .use(IonicVue)
  .use(router);
  
router.isReady().then(() => {
  app.mount('#app');
});


const firebaseConfig = {
  apiKey: "AIzaSyCtHFyfCRkBt8MX5LPFogBi8ssKSypkW0g",
  authDomain: "wrbapp.firebaseapp.com",
  projectId: "wrbapp",
  storageBucket: "wrbapp.appspot.com",
  messagingSenderId: "160377508482",
  appId: "1:160377508482:web:f651ccf2b242daf4879a9b",
  measurementId: "G-31HEXDSVPZ"
};


// Initialize Firebase
const firebase = initializeApp(firebaseConfig);

// eslint-disable-next-line
const analytics = getAnalytics(firebase);
// eslint-disable-next-line
const db = getFirestore(firebase);